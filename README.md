# ARIMA as Custom Function in WML

Example of a deployment of a ARIMA model using a custom function in WML

## Using `ibm_boto3` in new `default_py3.7` environment

`download_file()` with `ibm_boto3` seems to fail, and need additional configuration:

```
transferconfig = ibm_boto3.s3.transfer.TransferConfig(use_threads=False)
cos.download_file(Filename='hi.pkl', Key='hi.pkl', Bucket='my_bucket', Config=transferconfig)
```
the above code snippet can be used in custom python functions on `default_py3.7`